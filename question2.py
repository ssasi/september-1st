def rev_filename(file_name):
    rev_name = ".txt"
    for char in file_name:
        rev_name = char + rev_name
    return rev_name

with open("D:\september_1st\hello.txt") as f1:
    file1 = f1.readlines()
    print(file1)
    rev_data = []
    for i in range(1,len(file1)+1):
        line1 = file1[-i]
        rev_line1 = ""
        if i > 1:
            line1 = line1[:-1]
        for char in line1:
            rev_line1 = char + rev_line1
        rev_line1  = rev_line1 + "\n"
        rev_data.append(rev_line1)
    print(rev_data)
    
file_name = "hello"
rev_name = rev_filename(file_name)
with open("D:\september_1st\\"+rev_name, "w") as f2:
    for each_line1 in rev_data:
        f2.write(each_line1)
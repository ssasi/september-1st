def convert_to_number(input_list):
    number = ""
    for element in input_list:
        number =  number + str(element)
    return int(number)

def add_nums(num1, num2):
	return num1+num2
    
    
list_a = [1,2,3,4]
list_b = [8, 6]

num1 = convert_to_number(list_a)
num2 = convert_to_number(list_b)

result = add_nums(num1, num2)

print(result)